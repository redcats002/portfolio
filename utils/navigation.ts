export const externalNavigationTo = (url: string, type = "_blank") => {
  if (url.includes("https://")) return window.open(`${url}`, type);
  return window.open(`https://${url}`, type);
};
