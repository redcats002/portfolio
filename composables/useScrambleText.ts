export const useScrambleText = (
  el: Ref,
  finalText: string,
  duration = 90,
  repeat = false
) => {
  if (!process.client) return;
  let intervalId: NodeJS.Timeout; // Declare intervalId outside the functions
  const scrambleText = () => {
    let index = 0;
    intervalId = setInterval(() => {
      if (index === finalText.length) {
        clearInterval(intervalId);
        return;
      }
      const scrambledText = finalText
        .split("")
        .map((char, i) => (i <= index ? char : getRandomChar()))
        .join("");
      el.value = scrambledText;
      index++;
    }, duration);
  };

  const getRandomChar = () => {
    const characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()";
    return characters.charAt(Math.floor(Math.random() * characters.length));
  };

  const restartScramble = () => {
    clearInterval(intervalId);
    scrambleText();
  };
  onMounted(() => {
    scrambleText();
  });

  onUnmounted(() => {
    clearInterval(intervalId);
  });

  // Re-scramble text every demand milliseconds
  watchEffect(() => {
    if (repeat) {
      const intervalId = setInterval(() => {
        restartScramble();
      }, 10 * 1000);
      return () => {
        clearInterval(intervalId);
      };
    }
  });
};
