/** @type {import('tailwindcss').Config} */
export default {
  content: [],
  theme: {
    screens: {
      sm: "480px",
      md: "768px",
      lg: "976px",
      xl: "1440px",
      tablet: "640px",
      // => @media (min-width: 640px) { ... }

      laptop: "1024px",
      // => @media (min-width: 1024px) { ... }

      desktop: "1280px",
      // => @media (min-width: 1280px) { ... }
    },
    // colors: {
    //   transparent: "transparent",
    // },
    fontFamily: {
      IBM_PLEX_MONO: ["IBM Plex Mono", "sans-serif"],
      NOTO_SANS_THAI: ["Noto Sans Thai", "sans-serif"],
      ANTIC_SLAB: ["Antic Slab", "serif"],
    },
    extend: {
      spacing: {
        "128": "32rem",
        "144": "36rem",
      },
      borderRadius: {
        "4xl": "2rem",
      },
      colors: {
        primary: "#262626",
        secondary: "#D9D9D9",
      },
    },
  },
  plugins: [],
  prefix: "tw-",
  important: false,
  corePlugins: {
    preflight: false,
  },
  darkMode: "class",
};
