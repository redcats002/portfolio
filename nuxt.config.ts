// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    "@tresjs/nuxt",
    "@nuxtjs/google-fonts",
    "@nuxtjs/tailwindcss",
    "@nuxtjs/color-mode",
    "nuxt-primevue",
    "nuxt-icon",
    "@pinia/nuxt",
    "@nuxt/image",
    "@vueuse/nuxt",
  ],
  css: [
    "~/assets/css/global.css",
    "primevue/resources/themes/aura-light-green/theme.css",
  ],
  build: {
    transpile: ["gsap"],
  },
  tres: {
    devtools: true,
    glsl: true,
  },
  pinia: {
    storesDirs: ["./stores/**"],
  },
  googleFonts: {
    families: {
      "Noto+Sans+Thai": true,
      "IBM+Plex+Mono": true,
      "Antic+Slab": true,
    },
  },
  tailwindcss: {
    cssPath: ["~/assets/css/tailwind.css", { injectPosition: "first" }],
    configPath: "tailwind.config",
    exposeConfig: {
      level: 2,
    },
    config: {},
    viewer: true,
  },
  colorMode: {
    preference: "system", // default value of $colorMode.preference
    fallback: "light", // fallback value if not system preference found
    hid: "nuxt-color-mode-script",
    globalName: "__NUXT_COLOR_MODE__",
    componentName: "ColorScheme",
    classPrefix: "",
    classSuffix: "-mode",
    storageKey: "nuxt-color-mode",
  },
  primevue: {
    usePrimeVue: true,
    cssLayerOrder: "tailwind-base, primevue, tailwind-utilities",
  },
});
