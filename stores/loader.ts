export const useAppLoader = defineStore("useAppLoader", () => {
  const isPreloading = ref(true);
  const handlePreloader = (value: boolean) => {
    isPreloading.value = value;
  };
  return { isPreloading, handlePreloader };
});
